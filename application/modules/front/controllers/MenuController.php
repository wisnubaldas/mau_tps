<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MenuController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('menuGridModel','menuModel');
		
	}
	
	public function track_import()
	{
	    $this->_ajax();
		$this->parser->parse("track/import.tpl");
	}
	public function track_export()
	{
	    $this->_ajax();
		$this->parser->parse("track/export.tpl");
	}
	public function edit_menu()
    {
        $this->_ajax();
        $data['menuGrid'] = json_encode($this->menuModel->get_all());
        $this->parser->parse("adminPage/edit_menu.tpl",$data);
    }
    private function _ajax()
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

    }

}

/* End of file MenuController.php */
/* Location: ./application/modules/front/controllers/MenuController.php */