<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TpsController extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['gateinModel','kodeDokModel']);
	}
	public function gatein()
	{
		$data['title'] = ['Menu Manager','Page','Menu Manager','Menu List','index'];
		return $this->parser->parse('tps/index',$data);
	}
	public function gateinGrid()
	{
		  

		$start = $this->input->post('length');
		// $data = $this->gateinModel->with_kdDok()->as_array()->get_all();
		$total_posts = $this->gateinModel->count_rows(); // retrieve the total number of posts
		$post = $this->gateinModel->with_kdDok()->paginate($start,$total_posts); // paginate with 10 rows per page
		$all = $this->gateinModel->all_pages; 
		$prev = $this->gateinModel->previous_page; 
		$next = $this->gateinModel->next_page;
		print_r($all);
		// $xxx = json_encode(["draw"=>$this->input->post('draw'),
		// 				  "start"=>1,
		// 				  "recordsTotal"=>$total_posts,
		// 				  "recordsFiltered"=>count($post),
		// 				  "data"=>$post]);
		// return $this->output
  //           ->set_content_type('application/json')
  //           ->set_status_header(200)
  //           ->set_output($xxx);
	}
	public function gateKodeDok()
	{
		$id = $this->input->get('id');
		$data = $this->kodeDokModel->where('nilai',$id)->get_all();
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
	}
}

/* End of file TpsController.php */
/* Location: ./application/modules/front/controllers/TpsController.php */